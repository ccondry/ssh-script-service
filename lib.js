//Create a new instance
const SSH2Shell = require ('ssh2shell')
const db = require('./mongodb')
const ObjectID = require('mongodb').ObjectID

module.exports = {
  create: async function (host) {
    const job = {
      host,
      status: 'created',
      sessionText: null
    }
    const results = await db.insert('jobs', job)
    // get binary ID
    const id = results.insertedIds[0]
    // return hex ID
    return id.toHexString()
  },
  delete: async function (id) {
    const _id = ObjectID.createFromHexString(id)
    return await db.deleteOne('jobs', {_id})
  },
  get: async function (id) {
    const _id = ObjectID.createFromHexString(id)

    const job = await db.findOne('jobs', {_id})
    
    // redact password
    if (job !== null && job.host && job.host.server && job.host.server.password) {
      job.host.server.password = '*****'
    }

    return job
  },
  execute: async function (id) {
    const _id = ObjectID.createFromHexString(id)
    // look up job
    const job = await db.findOne('jobs', {_id})
    // handle 'su -' command
    job.host.onCommandProcessing = function( command, response, sshObj, stream ) {
      if (command === "su -" && response.indexOf("Password: ") != -1 && sshObj.suPassSent != true) {
        sshObj.commands.unshift("msg:Authenticating 'su -' command")
        //this is required to stop "bounce" without this the password would be sent multiple times
        sshObj.suPassSent = true
        stream.write(job.host.server.password + "\n")
      }
    }
    // create connection object
    const ssh = new SSH2Shell(job.host)

    // connect and run commands
    ssh.connect(function (sessionText) {
      // when finished, update the database with 'done' status and sessionText
      db.update('jobs', {_id}, {
        $set: {
          status: 'done',
          sessionText
        }
      })
    })

    // immediately update the database with 'processing' status
    db.update('jobs', {_id}, {
      $set: {
        status: 'processing',
        sessionText: null
      }
    })
  }
}
