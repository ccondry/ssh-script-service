# ssh-script-service
A HydraExpress microservice that can connect to SSH2 servers and run shell commands.

## Usage
Start the service with `npm start` or `node index`

To test, run the follwing commands:
create job: `hydra-cli rest ssh-script-service:[post]/jobs /temp/job.json`
the header will contain the relative location of the new job, for example `/jobs/59acff17c08ae666028e10a7`
execute job: `hydra-cli rest ssh-script-service:[post]/jobs/59acff17c08ae666028e10a7/execute`
get status: `hydra-cli rest ssh-script-service:[get]/jobs/59acff17c08ae666028e10a7`
