const hydraExpress = require('hydra-express')
const hydra = hydraExpress.getHydra()
const SSH2Shell = require ('ssh2shell')
const dotenv = require('dotenv')
const pkg = require('./package.json')
const lib = require('./lib')

// load .env file
dotenv.load()

// set up hydra and redis config
let hydraConfig = {
  hydra: {
    serviceName: pkg.name,
    serviceIP: process.env.hydra_service_ip || '',
    servicePort: process.env.hydra_service_port || 0,
    serviceType: process.env.hydra_service_type || '',
    serviceDescription: pkg.description,
    redis: {
      url: process.env.redis_url,
      port: process.env.redis_port,
      db: process.env.redis_db
    }
  }
}

// define routes
function onRegisterRoutes() {
  var express = hydraExpress.getExpress()
  var api = express.Router()

  // get job status
  api.get('/jobs/:id', async function(req, res) {
    try {
      const id = req.params.id
      console.log(`received request to get job ${id}`)
      const job = await lib.get(id)
      if (job === null) {
        console.log(`job ${id} is null`)
        return res.status(404).send()
      } else {
        console.log(`job ${id} found`)
        return res.status(200).send(job)
      }
    } catch (error) {
      console.log(error)
      return res.status(500).send(error)
    }
  })

  // api.get('/jobs/:id', function(req, res) {
  //   lib.get(req.params.id)
  //   .then(status => {
  //     console.log(status)
  //     res.status(200).send(status)
  //   })
  //   .catch(error => {
  //     console.log(error)
  //     res.status(500).send(error)
  //   })
  // })

  // create job
  api.post('/jobs', async function(req, res) {
    console.log(`create job request received`)
    try {
      // create job in database
      const id = await lib.create(req.body)
      // return success
      console.log(`job ${id} created`)
      const location = `/jobs/${id}`
      // set location header
      res.set('location', location)
      // send CREATED
      return res.status(201).send()
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  })

  // delete job
  api.delete('/jobs/:id', async function(req, res) {
    const id = req.params.id
    console.log(`delete job ${id} request received`)
    try {
      // delete job from database
      await lib.delete(id)
      // return success
      console.log(`job ${id} deleted`)
      // send ACCEPTED
      return res.status(202).send()
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  })

  // start job
  api.post('/jobs/:id/execute', async function(req, res) {
    console.log(`execute job ${req.params.id} request received`)
    try {
      // execute job
      lib.execute(req.params.id)
      // immediately return that we have started the job
      console.log(`job ${req.params.id} started`)
      // send ACCEPTED
      res.status(202).send()
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  })

  hydraExpress.registerRoutes({
    '': api
  })
}

// start up
hydraExpress.init(hydraConfig, onRegisterRoutes)
.then(serviceInfo => {
  console.log('serviceInfo', serviceInfo)
  // return 0
})
.catch(err => console.log('err', err))
